# frozen_string_literal: true

module TopsailService
  #
  # Controller concern for interacting with ServiceOnObject
  #
  # add to your resource's controller
  #   include TopsailService::ControllerActions
  #   service :my_service_action_name, MyServiceAction
  module ControllerActions
    extend ActiveSupport::Concern

    module ClassMethods
      private

      def service(service_path, service_model_class, policy_class: nil)
        service_form_action = "#{service_path}_form"

        define_method service_form_action do
          service_form(service_model_class, service_path: service_path, policy_class: policy_class)
        end

        define_method service_path do
          perform_service(service_model_class, service_path: service_path, policy_class: policy_class)
        end
      end
    end

    private

    def service_form(service_model_class, service_path:, policy_class:)
      @service = service_from_params(service_model_class)
      authorize(@service, policy_class: policy_class) # using query "#{service_path}_form?" since we are in that action
      render service_path
    end

    # also sets @record and @service
    def perform_service(service_model_class, service_path:, policy_class:)
      @service = service_from_params(service_model_class)
      authorize(@service, policy_class: policy_class) # using query "#{service_path}?" since we are in that action

      if @service.conduct
        flash.now.notice = response.headers['Content-Description'] =
          service_model_class.human_attribute_name(:performed, default: "#{service_model_class.model_name.human} performed")
        render "#{service_path}_success"
      else
        render status: :unprocessable_entity
      end
    end

    def service_from_params(service_model_class)
      service_params = params[service_model_class.model_name.param_key]&.permit! || {}
      service_model_class.new(service_params)
    end
  end
end
