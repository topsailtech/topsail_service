# frozen_string_literal: true

module TopsailService
  # subclasses should
  # - declare attr_accessor(s)
  # - (optional) declare validations
  # - implement :perform (no arguments)
  class Service
    include ActiveModel::Model

    def conduct
      conduct!
      true
    rescue ActiveModel::ValidationError => _e
      false
    end

    # @raise [ActiveModel::ValidationError] if validation fails
    def conduct!
      validate!
      perform
    end

    protected

    # you can still throw ActiveModel::ValidationError here, even though
    #   validation itself already happened.
    # Other exceptions will trickle up to your application code
    def perform
      raise 'Implemented by subclass'
    end
  end
end
