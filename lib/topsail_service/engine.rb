# frozen_string_literal: true

module TopsailService
  class Engine < ::Rails::Engine; end
end
