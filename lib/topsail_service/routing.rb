# frozen_string_literal: true

module TopsailService
  #
  # add to your /config/routes.rb
  #   concern :serviceable, TopsailService::Routing.new
  # and inside resource soemthing like
  #   concerns :serviceable, service_path: 'my_service_action'
  class Routing
    def call(mapper, options = {})
      service_path = options[:service_path]
      mapper.collection do
        mapper.get service_path, action: "#{service_path}_form"
        mapper.post service_path
      end
    end
  end
end
