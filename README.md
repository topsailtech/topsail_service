# TopsailService

This gem supports the creation of service objects that are based on
ActiveModel, thus provide validation and unified workflows.

## Installation

Add this line to your application's Gemfile:

```ruby
git_source(:topsail_gem){ |repo_name| "https://bitbucket.org/topsailtech/#{repo_name}.git" }

gem 'topsail_service', topsail_gem: 'topsail_service'
```

and `bundle install`.

## Usage
Example: a `WithdrawlService`.

### Write a `TopsailService::Service` model
- create a class `WithdrawlService < TopsailService::Service`. `app/services` is  good place to store the code.
- add `attr_accessor`s as needed.
- implement validations if needed
- implement `protected` instance method `perform()`. Your `perform` method may raise exceptions, but only exception `ActiveModel::ValidationError` will be automatically gracefully handled. All other exceptions will bubble up.

### Create `route`s and controller actions

Add to the top of your `config/routes.rb`
    `concern :serviceable, TopsailService::Routing.new`

Register the service in your resources section

```
resources 'accounts' do
  concerns :serviceable, service_path: 'withdrawl_service'
end
```

Add to your `AccountsController` controller

```
include TopsailService::ControllerActions
service :withdrawl_service, WithdrawlService
```

Of course your service actions don't have to be tied to an existing resource controller. You can create a separate controller for the service.

### Implement a Pundit Policy

Implement a Pundit policy `WithdrawlServicePolicy` (based on the service class name) with methods `withdrawl_service_form?` and `withdrawl_service?` (based on the service path defined in the controller's `service` call).
Alternatively you can use a different policy class by specifying a `policy_class:` argument on the controller's `service` call. Be aware that the `record` instance variable will still be a `WithdrawlService` instance though!

### Write a HTML for for editing the service attributes

Write a form for the service `app/views/accouts/withdrawl_service.html.erb`, like
```
<%= form_with(model: @service,
              url:   request.path,
              local: false) do |f| %>
    <%= render 'validation_errors', record: @service %>
    ...
    <div id="form_actions">
        <%= f.submit 'Withdraw!' %>
        <%= cancel_link %>
    </div>
<% end %>
```

For applictions that use the single page cruddy views gem, a form layout is available like so:
```
<%= render layout: 'topsail_service_form' do |f| %>
    All my form fields here, using FormBuilder `f`
<% end %>
```

You can customize the submit button via I18n (active model naming of the service model, or directly with I18n key `en.helpers.submit.withdrawl_service.create`)

### Write a `success` template
Implement a template `app/views/accouts/withdrawl_service_success.html.erb`.
Example:
```
<%= render 'index_tr', record: @service.account, cell_procs: index_cell_procs('accounts/index_cfg') %>
```

You can also customize the success growl with i18n (key `en.activemodel.attributes.withdrawl_service.performed`), or at least set a pretty service name in `en.activemodel.models.withdrawl_service.one`.



## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
